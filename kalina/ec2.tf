resource "aws_instance" "web" {
  ami           = "ami-0b69ea66ff7391e80"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  key_name = "dev"
  vpc_security_group_ids  = [aws_security_group.allow_ssh_from_home_only.id]
  user_data = file("rsc/cloud-init.yaml")
  tags = local.tags
}

resource "aws_security_group" "allow_ssh_from_home_only" {
  name        = "allow_ssh_from_home_only"
  description = "SSH only from home"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["174.6.109.61/32"]
  }
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.tags
}
